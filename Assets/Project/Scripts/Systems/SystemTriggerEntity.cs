using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using Component = UnityEngine.Component;

namespace Sr4BlackDev.Entities
{
//    [UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//    public partial class SystemTriggerEntity : SystemBase
//    {
//        protected override void OnCreate()
//        {
//            base.OnCreate();
//                     
//        }
//         
//        protected override void OnUpdate()
//        {
//            ComponentDataFromEntity<TestTrigerComponent> TriggerGravityFactorGroup =
//                GetComponentDataFromEntity<TestTrigerComponent>();
//            
//            TriggerGravityJob job = new TriggerGravityJob()
//            {
//                TriggerGravityFactorGroup = TriggerGravityFactorGroup,
//                manager = EntityManager,
//            };
//            
//        }
//
//        struct TriggerGravityJob : ITriggerEventsJob
//        {
//            [ReadOnly(true)] public ComponentDataFromEntity<TestTrigerComponent> TriggerGravityFactorGroup;
////            public ComponentDataFromEntity<PhysicsGravityFactor> PhysicsGravityFactorGroup;
////            public ComponentDataFromEntity<PhysicsVelocity> PhysicsVelocityGroup;
//            public EntityManager manager;
//            public void Execute(TriggerEvent triggerEvent)
//            {
//                
//                        
//                Entity projectile = Entity.Null;
//                Entity enemy = Entity.Null;
//                
//                if (TriggerGravityFactorGroup.HasComponent(triggerEvent.EntityA))
//                    projectile = triggerEvent.EntityA;
//                if (TriggerGravityFactorGroup.HasComponent(triggerEvent.EntityB))
//                    projectile = triggerEvent.EntityB;
//                
//                if (Entity.Null.Equals(projectile) 
//                    || Entity.Null.Equals(enemy)) return;
//                
//                Debug.Log("Va cham " + projectile);
//                
//
////                
////                Entity a = triggerEvent.EntityA;
////                Entity b = triggerEvent.EntityB;
////
////                bool isBodyATrigger = TriggerGravityFactorGroup.HasComponent(a);
////                bool isBodyBTrigger = TriggerGravityFactorGroup.HasComponent(b);
////                
////                if (isBodyATrigger && isBodyBTrigger)
////                    return;
////                bool isBodyADynamic = PhysicsVelocityGroup.HasComponent(a);
////                bool isBodyBDynamic = PhysicsVelocityGroup.HasComponent(b);
////                
////                if ((isBodyATrigger && !isBodyBDynamic) ||
////                    (isBodyBTrigger && !isBodyADynamic))
////                    return;
//
//            }
//        }
//
//        public void OnCreate(ref SystemState state)
//        {
//            
//        }
//
//        public void OnDestroy(ref SystemState state)
//        {
//            
//        }
//
//        public void OnUpdate(ref SystemState state)
//        {
//            
//            ComponentDataFromEntity<CharacterComponent> TriggerGravityFactorGroup = GetComponentDataFromEntity<CharacterComponent>(true),
//            state.Dependency = new TriggerGravityJob
//            {
//                TriggerGravityFactorGroup =  SystemAPI.GetComponentLookup<TriggerGravityFactor>(),
//                PhysicsGravityFactorGroup = SystemAPI.GetComponentLookup<PhysicsGravityFactor>(),
//                PhysicsVelocityGroup = SystemAPI.GetComponentLookup<PhysicsVelocity>(),
//            }.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);
//        }
//    }






//[BurstCompile]
    public struct CollisionTriggerJob : ITriggerEventsJob
    {
        public ComponentDataFromEntity<TestTrigerComponent> allPickups;
        //public ComponentDataFromEntity<CharacterComponent> characters;
        
        public EntityCommandBuffer entityCommandBuffer;
        public void Execute(TriggerEvent triggerEvent)
        {
            Entity entityA = triggerEvent.EntityA;
            Entity entityB = triggerEvent.EntityB;

            
            

            if (allPickups.HasComponent(entityA) && allPickups.HasComponent(entityB))
            {
                // Todo Logic A và B va chạm với nhau 
                Debug.Log("default");
                return;
            }
               
            if (allPickups.HasComponent(entityA) && !allPickups.HasComponent(entityB))
            {
                //Todo Logic A va chạm với Vật khác không chứa TestTrigger
                Debug.Log("TestTrigerComponent Trigger To "+ entityA);
                //entityCommandBuffer.DestroyEntity(entityA);

                //DestroyEntity(entityA, entityCommandBuffer);
            }
            else if (!allPickups.HasComponent(entityA) && allPickups.HasComponent(entityB))
            {
                //Todo Vật khác va chạm với TestTrigger
                
                //entityCommandBuffer.DestroyEntity(entityB);
                
                //DestroyEntity(entityB, entityCommandBuffer);

                if (allPickups[entityB].HasTrigged==false)
                {
                    Debug.Log("Was TestTrigerComponent "+ entityB);
                    TestTrigerComponent temp = allPickups[entityB];
                    temp.HasTrigged = true;
                    entityCommandBuffer.SetComponent(entityB, temp);
                }
//                if (!entityManager.GetComponentData<TestTrigerComponent>(entityB).HasTrigged)
//                {
//                    
//                    
//                }
            }
        }

        void DestroyEntity(Entity entity, EntityCommandBuffer commandBuffer)
        {
            commandBuffer.DestroyEntity(entity);
        }
    }
     [UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//    [UpdateBefore(typeof(SystemAnimator))]
    public partial class CollisionTriggerSystem : SystemBase
    {
        private StepPhysicsWorld stepPhysicsWorld;
        private EndSimulationEntityCommandBufferSystem commandBufferSystem;
        protected override void OnCreate()
        {
            stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
            commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override void OnUpdate()
        {
            var job = new CollisionTriggerJob
            {
                allPickups = GetComponentDataFromEntity<TestTrigerComponent>(),
//                allPlayers = GetComponentDataFromEntity<CharacterComponent>(true),
                entityCommandBuffer = commandBufferSystem.CreateCommandBuffer()
            };
            Dependency = job.Schedule(stepPhysicsWorld.Simulation, Dependency);
            Dependency.Complete();
            commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }




}
