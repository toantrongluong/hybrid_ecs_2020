using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Sr4BlackDev.Entities
{
    public class ITransform : IComponentData
    {
        public Transform Value;
    }

    public class IGameObject : IComponentData
    {
        public GameObject Value;
    }
}
