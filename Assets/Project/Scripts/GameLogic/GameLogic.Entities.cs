using System.Collections;
using System.Collections.Generic;
using Sr4BlackDev.Entities;
using Unity.Entities;
using UnityEngine;

namespace Sr4BlackDev
{
      public static partial class GameLogic
      {
            private static bool _setup;
            private static EntityManager _manager;
            public static EntityManager EntityManager
            {
                  get
                  {
                        if (!_setup)
                        {
                              _manager = World.DefaultGameObjectInjectionWorld.EntityManager;
                              _setup = true;
                        }

                        return _manager;
                  }
            }


      }

}

