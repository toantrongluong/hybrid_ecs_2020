using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


namespace Sr4BlackDev.Entities
{
    public class HybridConvertToEntity : MonoBehaviour
    {
        public Entity entity;
        void Awake()
        {
            entity = AddToEntityManager(GameLogic.EntityManager, gameObject);
        }
        public static Entity AddToEntityManager(EntityManager entityManager, GameObject gameObject)
        {
            Entity  entity = entityManager.CreateEntity();
            entityManager.SetName(entity, gameObject.name);
            entityManager.AddComponent<ITransform>(entity);
            entityManager.SetComponentData(entity, new ITransform(){Value = gameObject.transform});
            
            entityManager.AddComponent<IGameObject>(entity);
            entityManager.SetComponentData(entity, new IGameObject(){Value = gameObject});
            
            //TRANSFORM
            
            Translation translation = new Translation();
            translation.Value = gameObject.transform.position;
            entityManager.AddComponentData(entity, translation);

            Rotation rotation = new Rotation();
            rotation.Value = new quaternion(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z, gameObject.transform.rotation.w);
            entityManager.AddComponentData(entity, rotation);
            NonUniformScale scale = new NonUniformScale();
            scale.Value = gameObject.transform.localScale;
            entityManager.AddComponentData(entity, scale);
            LocalToWorld localToWorld = new LocalToWorld
            {
                Value = float4x4.TRS(translation.Value, rotation.Value, scale.Value)
            };
            entityManager.AddComponentData(entity, localToWorld);

            return entity;
            
            
            
        }
    }


}

