//
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using Sr4BlackDev;
//using Sr4BlackDev.Entities;
//using Unity.Entities;
//using Unity.Mathematics;
//using Unity.Physics;
//using Unity.Physics.Authoring;
//using Unity.Transforms;
//using UnityEngine;
//
//public class CharacterAuthoring : MonoBehaviour
//{
//    private Entity ent;
//    public ICharacter icharacter;
//    
//    //public PhysicsShapeAuthoring shapeAuthorig;
//    //public PhysicsBodyAuthoring bodyAuthoring;
//
//    public BoxData boxData;
//    private void Start()
//    {
//        CreateEntity();
//    }
//    void CreateEntity()
//    {
//        EntityManager manager = World.DefaultGameObjectInjectionWorld.EntityManager;
//        ent = manager.CreateEntity();
//        manager.SetName(ent,gameObject.name);
//        CharacterComponent character = new CharacterComponent();
//        character.index = ent.Index;
//        character.value = 0;
//        ent.AddCharacterComponentData(manager, character);
//        ent.AddCharacter(icharacter);
//
//        TempData temp = new TempData();
//        temp.character = icharacter;
//        manager.AddComponent<TempData>(ent);
//        manager.SetComponentData(ent, temp);
//        
//        
//        //TRANSFORM
//
//        Translation translation = new Translation();
//        translation.Value = transform.position;
//        manager.AddComponentData(ent, translation);
//
//        Rotation rotation = new Rotation();
//        rotation.Value = new quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
//        manager.AddComponentData(ent, rotation);
//        NonUniformScale scale = new NonUniformScale();
//        scale.Value = transform.localScale;
//        manager.AddComponentData(ent, scale);
//        LocalToWorld localToWorld = new LocalToWorld
//        {
//            Value = float4x4.TRS(translation.Value, rotation.Value, scale.Value)
//        };
//        manager.AddComponentData(ent, localToWorld);
//        manager.AddComponent<CopyTransformFromGameObject>(ent);
//////NEW PHYSIC
////        PhysicsVelocity physicsVelocity = new PhysicsVelocity
////        {
////            Linear = float3.zero,
////            Angular = float3.zero,
////        };
////        manager.AddComponentData(ent, physicsVelocity);
////
////
////        BoxGeometry geometry = new BoxGeometry()
////        {
////            Center = boxData.Center,
////            Size = boxData.Size,
////            Orientation = quaternion.Euler(boxData.EulerAngle)
////        };
////        
////        
////        
////        
////        
////        
////        
////        CollisionFilter filter = new CollisionFilter()
////        {
////            BelongsTo = boxData.BelongTo.Value,// shapeAuthorig.BelongsTo.Value,
////            CollidesWith = boxData.CollidesWith.Value,//shapeAuthorig.CollidesWith.Value,
////        };
////        Unity.Physics.Material material = Unity.Physics.Material.Default;
////        material.CollisionResponse = boxData.CollisionResponse;// CollisionResponsePolicy.RaiseTriggerEvents;
////
////        BlobAssetReference<Unity.Physics.Collider> sphereCollider = Unity.Physics.BoxCollider.Create(geometry, filter, material);
////
////        PhysicsCollider _physicsCollider = new PhysicsCollider();
////        _physicsCollider.Value = sphereCollider;
////        manager.AddComponentData(ent, _physicsCollider);
////        
////        manager.AddComponent<PhysicsWorldIndex>(ent);
////        
//        
//        
//    }
//}
////public struct PhysicsBodyComponent : IComponentData
////{
////    public PhysicsVelocity Velocity;
////}
////public struct PhysicsShapeComponent : IComponentData
////{
////    public BlobAssetReference<Unity.Physics.Collider> Collider;
////}
//[System.Serializable]
//public class BoxData
//{
//    [Header("Box Geometry")]
//    public float3 Center;
//    public float3 Size;
//    public float BevelRadius;
//    public float3 EulerAngle;
//    [Header("Collision Filter")]
//    public PhysicsCategoryTags BelongTo;
//    public PhysicsCategoryTags CollidesWith;
//    [Header("Physics Material")]
//    public CollisionResponsePolicy CollisionResponse;
//}
