using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Sr4BlackDev.Entities
{
    public struct TestTrigerComponent : IComponentData
    {
        public bool HasTrigged;
    }
  

}
