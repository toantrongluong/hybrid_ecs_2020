using System.Collections;
using System.Collections.Generic;
using Sr4BlackDev.Entities;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Sr4BlackDev.Authoring
{
    
    public class TestTriggerAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {

        
        
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new TestTrigerComponent(){HasTrigged = false});
            dstManager.AddComponentData(entity, new CopyTransformFromGameObject());
        }
    }

}
